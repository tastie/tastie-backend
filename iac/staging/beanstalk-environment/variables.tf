variable "loadbalancer_certificate_arn" { type = string }

variable "google_place_apikey" { type = string }
variable "postgres_dbname" { type = string }
variable "postgres_host" { type = string }
variable "postgres_password" { type = string }
variable "postgres_username" { type = string }
variable "sendinblue_apikey" { type = string }
variable "listen_port" { type = string }
variable "mongo_url" { type = string }